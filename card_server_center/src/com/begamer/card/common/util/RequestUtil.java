package com.begamer.card.common.util;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @ClassName: RequestUtil
 * @author gs
 * @date Nov 10, 2011 3:59:39 PM
 */
public class RequestUtil
{
	public static String readParams(HttpServletRequest request)
	{
		InputStream inputStream=null;
		try
		{
			inputStream=request.getInputStream();
			if (inputStream != null) 
			{
				ByteArray ba=new ByteArray();
				byte[] temp=new byte[1024];
				int length=-1;
				while((length=inputStream.read(temp))>0)
				{
					for(int i=0;i<length;i++)
					{
						ba.writeByte(temp[i]);
					}
				}
				return new String(ba.readByteArray());
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(inputStream!=null)
			{
				try
				{
					inputStream.close();
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return "";
	}
	
	/**
	 * 获取json信息
	 * lt@2013-9-5 下午03:34:06
	 * @param request
	 * @return
	 */
	public static JSONObject getJsonObject(HttpServletRequest request) throws Exception
	{
		String param=readParams(request);
		return JSON.parseObject(param);
	}
	
	/**
	 * 设置返回信息
	 * lt@2013-9-5 下午03:40:16
	 * @param request
	 * @param msg
	 */
	public static boolean setResponseMsg(HttpServletRequest request,String msg) throws Exception
	{
		setResult(request,msg);
		return true;
	}
	
	public static void setResult(HttpServletRequest request,String value)
	{
		request.setAttribute("result", value);
	}
	
}