package com.begamer.card.common.util.binRead;

public interface PropertyReader {
	void addData();
	void resetData();
	void parse(String[] ss);
}
