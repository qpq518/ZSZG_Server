package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class BattleLogJson extends BasicJson
{
	public List<String> bs;//==logs:unitId-cost-maxEnergy==//
	public int r;//result:1胜利,2失败,3新手演示战斗//
	public String gm;//goldMul金币倍数//
	public int bNum;//战斗场次
	public int sNum;//星级
	public int bonus;//是否出现bonus(0，未出现，1出现)
	public int round;//==经历回合数==//
	
	public List<String> getBs()
	{
		return bs;
	}
	public void setBs(List<String> bs)
	{
		this.bs = bs;
	}
	public int getR()
	{
		return r;
	}
	public void setR(int r)
	{
		this.r = r;
	}
	public String getGm()
	{
		return gm;
	}
	public void setGm(String gm)
	{
		this.gm = gm;
	}
	public int getBNum()
	{
		return bNum;
	}
	public void setBNum(int num)
	{
		bNum = num;
	}
	public int getSNum() {
		return sNum;
	}
	public void setSNum(int sNum) {
		this.sNum = sNum;
	}
	public int getBonus() {
		return bonus;
	}
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}
	public int getRound()
	{
		return round;
	}
	public void setRound(int round)
	{
		this.round = round;
	}
	
}
