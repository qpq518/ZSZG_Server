package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.ActivityGElement;

public class ActivityGResultJson extends ErrorJson{

	public List<ActivityGElement> age;//公告活动列表

	public List<ActivityGElement> getAge()
	{
		return age;
	}

	public void setAge(List<ActivityGElement> age)
	{
		this.age = age;
	}
	
}
