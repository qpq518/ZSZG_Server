package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class NewUnitskillResultJson extends ErrorJson
{
	public String unitskills;//新解锁的合体技id&id

	public String getUnitskills() {
		return unitskills;
	}

	public void setUnitskills(String unitskills) {
		this.unitskills = unitskills;
	}
	
	
	
}
