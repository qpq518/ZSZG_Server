package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class BattleLogResultJson extends ErrorJson
{
	/**1胜利,2失败**/
	public int r;//result//
	
	/**战前**/
	//player
	public int lv0;//playerLevel0//
	public int ce0;//playerCurExp0//
	public int me0;//playerMaxExp0//
	
	//角色卡,格式:cardId-level-curExp-maxExp-hp-atk-def
	public List<String> cs0;
	/**战后**/
	//player
	public int lv1;//playerLevel1//
	public int ce1;//playerCurExp1//
	public int me1;//playerMaxExp1//
	
	//角色卡,格式:cardId-level-curExp-maxExp-hp-atk-def
	public List<String> cs1;
	//奖励
	public int ag;//addGold//
	/**格式:type-info**/
	public List<String> ds;//dropsInfo//
	/**金币倍数**/
	public String gm;//goldMul//
	//解锁模块    id-是否解锁(0未解锁，1解锁)
	public String [] s;
	
	public int hi;//支援玩家playerId//
	public String hn;//支援玩家名字//
	public int power0;//升级前体力值
	public int power1;//升级后体力值
	
	
	public int getR()
	{
		return r;
	}
	public void setR(int r)
	{
		this.r = r;
	}
	public int getLv0()
	{
		return lv0;
	}
	public void setLv0(int lv0)
	{
		this.lv0 = lv0;
	}
	public int getCe0()
	{
		return ce0;
	}
	public void setCe0(int ce0)
	{
		this.ce0 = ce0;
	}
	public int getMe0()
	{
		return me0;
	}
	public void setMe0(int me0)
	{
		this.me0 = me0;
	}
	public List<String> getCs0()
	{
		return cs0;
	}
	public void setCs0(List<String> cs0)
	{
		this.cs0 = cs0;
	}
	public int getLv1()
	{
		return lv1;
	}
	public void setLv1(int lv1)
	{
		this.lv1 = lv1;
	}
	public int getCe1()
	{
		return ce1;
	}
	public void setCe1(int ce1)
	{
		this.ce1 = ce1;
	}
	public int getMe1()
	{
		return me1;
	}
	public void setMe1(int me1)
	{
		this.me1 = me1;
	}
	public List<String> getCs1()
	{
		return cs1;
	}
	public void setCs1(List<String> cs1)
	{
		this.cs1 = cs1;
	}
	public int getAg()
	{
		return ag;
	}
	public void setAg(int ag)
	{
		this.ag = ag;
	}
	public List<String> getDs()
	{
		return ds;
	}
	public void setDs(List<String> ds)
	{
		this.ds = ds;
	}
	public String getGm()
	{
		return gm;
	}
	public void setGm(String gm)
	{
		this.gm = gm;
	}
	public String[] getS() 
	{
		return s;
	}
	public void setS(String[] s) 
	{
		this.s = s;
	}
	public int getHi()
	{
		return hi;
	}
	public void setHi(int hi)
	{
		this.hi = hi;
	}
	public String getHn()
	{
		return hn;
	}
	public void setHn(String hn)
	{
		this.hn = hn;
	}
	public int getPower0() {
		return power0;
	}
	public void setPower0(int power0) {
		this.power0 = power0;
	}
	public int getPower1() {
		return power1;
	}
	public void setPower1(int power1) {
		this.power1 = power1;
	}
	
}