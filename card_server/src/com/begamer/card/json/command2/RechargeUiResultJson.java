package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class RechargeUiResultJson extends ErrorJson
{
	public int vipLv;//vip等级
	public int vipCost;//vip当前vip点
	public int sCost;//vip升至下一级的总点数
	public int vipMonthType;//是否月卡玩家   0否 1是
	public int vipMonthDay;//如果是月卡玩家，月卡剩余天数
	public List<String> ids;//需要显示描述的充值id
	public List<String> giftIds;//可以购买的vip礼包ids 
	
	public int getVipLv() {
		return vipLv;
	}
	public void setVipLv(int vipLv) {
		this.vipLv = vipLv;
	}
	
	public int getVipCost() {
		return vipCost;
	}
	public void setVipCost(int vipCost) {
		this.vipCost = vipCost;
	}
	public int getSCost() {
		return sCost;
	}
	public void setSCost(int sCost) {
		this.sCost = sCost;
	}
	public int getVipMonthType() {
		return vipMonthType;
	}
	public void setVipMonthType(int vipMonthType) {
		this.vipMonthType = vipMonthType;
	}
	public int getVipMonthDay() {
		return vipMonthDay;
	}
	public void setVipMonthDay(int vipMonthDay) {
		this.vipMonthDay = vipMonthDay;
	}
	public List<String> getIds() {
		return ids;
	}
	public void setIds(List<String> ids) {
		this.ids = ids;
	}
	public List<String> getGiftIds() {
		return giftIds;
	}
	public void setGiftIds(List<String> giftIds) {
		this.giftIds = giftIds;
	}
	
	
}
