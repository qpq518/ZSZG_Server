package com.begamer.card.model.pojo;

public class Activity
{
	public int id;
	public int activityId;//活动id
	public String activityName;//活动名称
	public int weight;//权值
	public int type;
	public String sttime;
	public String endtime;
	public int sell;
	
	public static Activity createActivity(int activityId,String activityName,int weight,int type)
	{
		Activity activity = new Activity();
		activity.setActivityId(activityId);
		activity.setActivityName(activityName);
		activity.setWeight(weight);
		activity.setType(type);
		activity.setSell(0);
		return activity;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getActivityId() {
		return activityId;
	}
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
	public int getSell() {
		return sell;
	}
	public void setSell(int sell) {
		this.sell = sell;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	public String getSttime() {
		return sttime;
	}

	public void setSttime(String sttime) {
		this.sttime = sttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	
	
}
