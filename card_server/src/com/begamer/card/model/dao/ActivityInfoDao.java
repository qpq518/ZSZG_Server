package com.begamer.card.model.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.log.DbErrorLogger;
import com.begamer.card.model.pojo.Activity;
import com.begamer.card.model.pojo.ActivityInfo;

public class ActivityInfoDao extends HibernateDaoSupport{
	
	private static final Logger dbLogger=DbErrorLogger.logger;
	
	public void add(ActivityInfo activityInfo)
	{
		try
		{
			getHibernateTemplate().save(activityInfo);
		}
		catch (RuntimeException e)
		{
			throw e;
		}
	}
	public int findByActivityId(int activityId)
	{
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from Activity a where a.activityId=?");
			query.setInteger(0, activityId);
			return query.list().size();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	public  void delActivityInfo(int id)
	{
		Session session = getSession();
		try
		{
			Query q = session.createQuery("delete from ActivityInfo a where a.id=?");
			q.setInteger(0, id);
			q.executeUpdate();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	public Activity getActivity(int activityId)
	{
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from Activity a where a.activityId=?");
			query.setInteger(0, activityId);
			return (Activity) query.uniqueResult();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	@SuppressWarnings("unchecked")
	public List<ActivityInfo> getActivityInfoByActivityId(int activityId)
	{
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from ActivityInfo a where a.activityId=?");
			query.setInteger(0, activityId);
			return query.list();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	public void delActivity(int activityId)
	{
		Session session = getSession();
		try
		{
			List<ActivityInfo> list = getActivityInfoByActivityId(activityId);
			if (list != null && list.size() > 0)
			{
				Query query = session.createQuery("delete from ActivityInfo a where a.activityId=?");
				query.setInteger(0, activityId);
				query.executeUpdate();
			}
			Query query = session.createQuery("delete from Activity a where a.activityId=?");
			query.setInteger(0, activityId);
			query.executeUpdate();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	public void addActivity(Activity activity)
	{
		try
		{
			getHibernateTemplate().save(activity);
		}
		catch (RuntimeException e)
		{
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Activity> find()
	{
		Session session = getSession();
		try
		{
			  Query query = session.createQuery("from Activity a");
			  return query.list();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ActivityInfo> find2()
	{
		try
		{
			return getHibernateTemplate().find("from ActivityInfo a");
		}
		catch (RuntimeException e)
		{
			throw e;
		}
	}
	
	public void upActivityInfo(ActivityInfo activityInfo)
	{
		try
		{
			getHibernateTemplate().merge(activityInfo);
		}
		catch (RuntimeException e)
		{
			throw e;
		}
	}
	public void upActivity(Activity activity)
	{
		try
		{
			getHibernateTemplate().merge(activity);
		}
		catch (RuntimeException e)
		{
			dbLogger.debug("update Activity failed" + e);
			throw e;
		}
	}
	
	public Activity getTreasureCanDate(int type)
	{
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from Activity a where a.type=?");
			query.setInteger(0, type);
			return (Activity) query.uniqueResult();
		}
		catch (RuntimeException e)
		{
			dbLogger.debug("查询活动时间出错",e);
			throw e;
		}
		finally
		{
			session.close();
		}
	}
}
