package com.begamer.card.model.view;

public class CardView {

	private int id;
	private String name;
	private int level;
	private int curExp;
	
	public static CardView createCardView(int id,String name,int level,int curExp)
	{
		CardView cardView = new CardView();
		cardView.setId(id);
		cardView.setName(name);
		cardView.setLevel(level);
		cardView.setCurExp(curExp);
		return cardView;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public int getCurExp()
	{
		return curExp;
	}

	public void setCurExp(int curExp)
	{
		this.curExp = curExp;
	}

}
