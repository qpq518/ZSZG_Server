package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class BloodCostData implements PropertyReader
{
	public int time;
	public int cost;
	
	private static HashMap<Integer, BloodCostData> data =new HashMap<Integer, BloodCostData>();
	@Override
	public void addData()
	{
		data.put(time, this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static BloodCostData getBloodCostData(int num)
	{
		return data.get(num);
	}

}
