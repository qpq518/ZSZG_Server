package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class EquipupGradeData implements PropertyReader{
	/**等级**/
	public int level;
	/**消耗金币**/
	public int cost;
	/**几率1x**/
	public int probability1;
	/**几率3x**/
	public int probability3;
	/**出售价格**/
	public int sell;
	
	private static HashMap<Integer, EquipupGradeData> data =new HashMap<Integer, EquipupGradeData>();
	@Override
	public void addData()
	{
		data.put(level, this);
	}
	@Override
	public void resetData()
	{
		data.clear();
	}
	@Override
	public void parse(String[] ss)
	{
		
	}
	public static EquipupGradeData getData(int level)
	{
		return data.get(level);
	}

}
