package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MissionCostData implements PropertyReader
{
	public int id;
	public int times;
	public int type;
	public int viplevel;
	public int cost;

	private static HashMap<Integer, MissionCostData> data =new HashMap<Integer, MissionCostData>();
	private static List<MissionCostData> dataList =new ArrayList<MissionCostData>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	
	/**根据id获取一个data**/
	public static MissionCostData getMissionCostData(int index)
	{
		return data.get(index);
	}
	
	public static HashMap<Integer ,MissionCostData> getMissionDatasByType(int type)
	{
		HashMap<Integer, MissionCostData> mcMap =new HashMap<Integer, MissionCostData>();
		for(MissionCostData mcData:data.values())
		{
			if(mcData.type ==type)
			{
				mcMap.put(mcData.times, mcData);
			}
		}
		return mcMap;
	}
	
	public static Integer getDataSize()
	{
		return dataList.size();
	}
	
	/**根据vip等级获取最大size**/
	public static Integer getMaxSize(int viplv)
	{
		for(int i=0;i<dataList.size();i++)
		{
			if(dataList.get(i).viplevel>viplv)
			{
				return i+1;
			}
		}
		return -1;
	}
}
