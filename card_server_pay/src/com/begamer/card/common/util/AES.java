package com.begamer.card.common.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AES {

    /**
     * AES解密算法
     * 
     * @param input
     * @param offset
     * @param length
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private static final String KEY = "rQJ6jPgOO/E=rQJ6";

    public static byte[] aesDecrypt(byte[] input, int offset) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

	byte[] put = new byte[input.length];
	System.arraycopy(input, offset, put, 0, input.length);

	SecretKeySpec sS = new SecretKeySpec(KEY.getBytes(), "AES");
	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	cipher.init(Cipher.DECRYPT_MODE, sS);
	byte[] decrypted = cipher.doFinal(put);
	return decrypted;
    }

    public static byte[] aesDecryptForPay(String key, byte[] input, int offset) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

	byte[] put = new byte[input.length];
	System.arraycopy(input, offset, put, 0, input.length);

	SecretKeySpec sS = new SecretKeySpec(key.getBytes(), "AES");
	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	cipher.init(Cipher.DECRYPT_MODE, sS);
	byte[] decrypted = cipher.doFinal(put);
	return decrypted;
    }

    public static byte[] aesEncrypt(byte[] input, int offset) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

	byte[] put = new byte[input.length];
	System.arraycopy(input, offset, put, 0, input.length);

	SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes(), "AES");
	Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
	c.init(Cipher.ENCRYPT_MODE, skeySpec);
	byte[] encrypt = c.doFinal(put);
	return encrypt;
    }

    public static byte[] aesEncryptWithKey(String key, byte[] input, int offset) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

	byte[] put = new byte[input.length];
	System.arraycopy(input, offset, put, 0, input.length);

	SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
	Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
	c.init(Cipher.ENCRYPT_MODE, skeySpec);
	byte[] encrypt = c.doFinal(put);
	return encrypt;
    }

    public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
	
    String hehe = "{\"userId\":\"cacawang\",\"gameId\":\"26\",\"reqtime\":1346742112296 ,\"state\":\"1\",\"consumeValue\":\"40\",\"extraData\":\"50292\",\"gameServerZone\":\"1\"}";
//	String hehe = "{\"gameServerZone\":\"3\",\"userId\":\"cacawang\",\"reqtime\":1346742112296 ,\"state\":\"1\",\"consumeValue\":\"200.00\",\"extraData\":\"10\",\"gameId\":\"1\"}";
//	userId 蜂巢账号 gameId游戏代号（对应末日之光） state状态1表示消费成功  consumeValue消费金额 extraData玩家playerId gameId游戏服务器ID1世界之树 3诸神黄昏
	String key = "BEGAMER2012mrzg1";//与蜂巢约定的密钥
	byte[] b = hehe.getBytes("utf-8");
	byte[] afterb = Base64.encodeBase64(aesEncryptWithKey(key, b, 0));
	System.out.println("加密后：" + new String(afterb, "utf-8"));
//	byte[] afterd = aesDecryptForPay(key, Base64.decodeBase64(afterb), 0);
//	System.out.println("解密后：" + new String(afterd, "utf-8"));
	
//	String key = "BEGAMER2012mrzg1";
//	String temp = "E7tiZDUCO2V7dEpPfkinGT0uFlYZrLo4hcu67PGtjsNMHU8ojQ5esTo/GNNI3XnMPyMLrnWVTu/y8lryV4PJqMUuR7nWsiOd2pk6+qlvHE3gtD2+vbQGF4rYxNHriwgBAj3PhlGVqdhavPkTtAMFKNO9TxymR+60pMJeEBwq638s1NX3E2cdIWnSrn0fNKBPpF4BVclj3TsF/23UKaM80g==";
//	byte[] tempbyte = aesDecryptForPay(key, Base64.decodeBase64(temp.getBytes()), 0);
//	System.out.println("解密后：" + new String(tempbyte, "utf-8"));
	

    }
}
