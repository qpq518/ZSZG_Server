package com.begamer.card.log;

import org.apache.log4j.Logger;

/**
 * GM日志
 * @author gehz
 *
 */
public class ManagerLogger {

	public static final Logger logger = Logger.getLogger("ManagerLogger");
	
	public static void print(String s)
	{
		logger.info(s);
	}
}
